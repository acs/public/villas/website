#DEPLOY_USER ?= fine-aachenadmin
#DEPLOY_PASS ?= $(shell pass rwth/acs/fein-webspace)
#DEPLOY_URL ?= https://web-upload.rwth-aachen.de/fine-aachen/new/

DEPLOY_USER ?= deploy
DEPLOY_HOST ?= acs-os-fein-website
DEPLOY_PATH ?= /var/www/villas/website/

SRCDIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
BUILDDIR ?= $(SRCDIR)/build

RSYNC_OPTS ?= --archive --delete --verbose --ignore-missing-args --copy-links --chown $(DEPLOY_USER):$(DEPLOY_USER)
DAVIX_OPTS ?= -r10 --userlogin $(DEPLOY_USER) --userpass $(DEPLOY_PASS)
JEKYLL_OPTS ?= -s $(SRCDIR) -d $(BUILDDIR)

build:
	jekyll build $(JEKYLL_OPTS)

deploy: deploy-rsync

deploy-rsync:
	rsync $(RSYNC_OPTS) $(BUILDDIR)/ $(DEPLOY_USER)@$(DEPLOY_HOST):$(DEPLOY_PATH)

deploy-webdav:
	# Davix returns 255 for some reason...
	davix-put $(DAVIX_OPTS) $(BUILDDIR)/ $(DEPLOY_URL) || true

.PHONY: build deploy deploy-rsync deploy-webdav
