---
layout: post
title: The VILLAS development continues
author: Steffen Vogel
subtitle: The VILLAS project is officially ended. However main product of the project "VILLASframework" is still under ongoing development and used in new research projects.
tags: project
---

# The VILLAS development continues..

The VILLAS project is officially ended. However main product of the project "VILLASframework" is still under ongoing development and used in new research projects.

Up-to-date information about the software developments and ongoing research around VILLAS can be found at the [FEIN e.V project web-site](https://fein-aachen.org/projects/villas-framework/).

A more in-depth description of the project can also be found at the [JARA Energy Annual Report](https://www.jara.org/de/forschung/jara-energy/news/detail/Annual-Rcyeport-2015?file=files/jara/downloads/JARA-ENERGY/Jahresbericht_JE_2015.pdf).

This project is now archived. No further updates are to be expected.
