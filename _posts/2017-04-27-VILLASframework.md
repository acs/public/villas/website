---
layout: post
title: VILLASframework
author: Steffen Vogel
tags: software
subtitle: Released as open source software
---

# VILLASframework

<img src="{{ site.baseurl }}/img/villas_framework.png" align="right" width="90" />

VILLASframework has been released as open source software under the GPLv3 license.

Details about the project can be found on its [project page](https://www.fein-aachen.org/projects/villas-framework/).
