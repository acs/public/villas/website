---
layout: page
title: "About"
description: "Why VILLAS?"
header-img: "img/home-bg.jpg"
order: 2
permalink: /about/
---

VILLAS is developing a methodology for virtual interconnection of hardware and software assets available at geographically dispersed institutions of RWTH Aachen University and Forschungszentrum Jülich. RWTH Aachen University and Forschungszentrum Jülich are already partners in the Jülich-Aachen Research Alliance (JARA) which integrates the expertise of the two institutions in emerging research areas. In this context, the JARA-Energy interdisciplinary initiative, which develops cooperative solutions for sustainable energy of the future, needs integration of research infrastructures and benefits from the outcomes of VILLAS.

VILLAS encompasses fundamental research and practical application, from system level down to component level in energy sector. VILLAS aims at bringing together individual competences and outstanding facilities by creating an integrated research infrastructure for fully coordinated and interdisciplinary research. VILLAS research infrastructure consists of the virtual interconnection of facilities of individual institutions over a communication medium. This enables using them together in individual experiments, despite their geographically dispersed locations. To achieve this VILLAS researchers are going to design and implement a set of VILLAS interfaces and VILLAS services for flexible integration of hardware and software assets.

Virtual integration of facilities over a communication medium in a transparent and flexible way is a challenge in itself. Furthermore, diversity of facilities and a wide range of expertise of individual groups involved in VILLAS create additional challenges. Thus, VILLAS research infrastructure will be developed in two phases. The ongoing Phase I represents a learning phase to gain knowledge and experience for Phase II. The Phase II includes the design and development of the VILLAS concept on a large scale in order to fully exploit all available facilities.

Phase I will specify the entire infrastructure and will provide a proof-of-concept demonstration. The specification of the VILLAS infrastructure will be outlined in terms of available hardware and software assets and typical applications of individual facilities with the goal to determine overall infrastructure capabilities and use cases. In this context, eleven institutes from RWTH and FZ Jülich are involved in VILLAS Phase I. Among them, ACS, EBC and PGS represent E.ON ERC infrastructure. Particular focus will be on determining needs and potential for enhancing the capabilities of individual facilities by enabling large-scale scenarios, system level and interdisciplinary studies, which would not be feasible for individual institution. At the same time, this phase will identify and analyze challenges and obstacles in developing virtually interconnected research environment arising from diversity of the involved facilities.

<figure>
<img src="../img/villas.png" width="90%">
</figure>

VILLAS will develop, within Phase I, the prototype as a proof-of-concept demonstration. This preliminary prototype involves three facilities, namely, the real-time simulation laboratory at ACS, High-Power Test Bench at PGS and Center for Wind Power Drives (CWD). Furthermore, the prototype includes a cloud platform to enable high-level interfaces, such as online and remote monitoring of the execution of experiments. User interface allows for post-processing of simulation results for further analysis as well as for parameter definition of particular simulation models. The prototype aims at demonstrating applicability and unique benefits of the VILLAS concept in realizing a multi-terminal virtual infrastructure and flexible monitoring of the experiments.

The project has received funding from JARA-ENERGY. Jülich-Aachen Research Alliance (JARA) is an initiative of RWTH Aachen University and Forschungszentrum Jülich. 	
