---
layout: page
title: "Disclaimer"
description: ""
header-img: "img/home-bg.jpg"
order: 14
permalink: /disclaimer/
---

If you provide personal data via this website, these will be processed in compliance with the Data Protection Act of North Rhine-Westphalia (DSG NRW). RWTH Aachen University has appointed a data protection officer who is responsible for administering the Act and who advises on the respective duties.

# Collection, Processing and Use of Personal Data

On principle, you can use our website without revealing your identity. By sending any information to RWTH (through a contact form, for example) you consent to this information being processed for our own purposes (e.g. sending of desired information, processing for advertising or for market opinion research) unless you declared an objection. You have the right to withdraw your consent at any time in the future, to immediate effect.
Upon a visit to our website the following data will be stored exclusively for statistical purposes, and the identity of the user is being kept anonymous:

- the name of the file requested,
- the date and time of the request,
- the amount of data transferred,
- IP number

We will not give any stored data to third parties – neither for commercial nor non-commercial purposes.

# Responsibility for our Web Content

RWTH Aachen University makes all reasonable efforts to ensure that the contents of its web site are up-to-date, complete and accurate. Despite these efforts, the occurrence of errors or omissions cannot be completely ruled out.
As a provider of teleservices and media services we are responsible only for own contents according to § 7 of the German Telemedia Act (TMG). RWTH Aachen University does not accept liability for the relevance, accuracy or completeness of the content provided on its web site unless the error or inaccuracy occurred intentionally or through gross negligence. This refers to any loss, additional costs or damage of any kind suffered as a result of the use of material provided by this web site.

# Responsibility for External Content

Some hyperlinks on this website link to contents which are not operated by RWTH but provided by third parties. Please note that RWTH Aachen University is not responsible for contents offered by other organisations.

# Copyright

The Layout of the Homepage, graphics and pictures used and the collection of individual contributions are protected by copyright.
The RWTH reserves all rights including the rights of photomechanical reproduction, the duplication and distribution via special processes (e.g. data processing, data carriers, data networks).

# Any Questions?

If you have any questions concerning the processing of your personal data, please contact our data protection officer who will be glad to assist you with your query.

# Legal Validity

This general disclaimer is part of the contents provided by the web site of RWTH Aachen University. If any of the terms and conditions is found to be invalid by reason of the relevant laws then the remaining terms and conditions shall remain in full effect.


