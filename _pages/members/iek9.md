---
layout: page
title: "IEK-9"
description: "Institute of Energy and Climate Research Fundamental Electrochemistry, Forschungszentrum Jülich"
header-img: "img/home-bg.jpg"
order: 6
permalink: /members/iek9/
member: iek9
---

### Key areas of expertise/research

* electrochemical energy conversion (SOFC/SOEC)
* electrochemical energy storage (batteries)

### Related Projects

* Verbundvorhaben SOFC Degradation: Analyse der Ursachen und Entwicklung von Gegenmaßnahmen
* Development of new electrode materials and understanding of degradation mechanisms on Solid Oxide High Temperature Electrolysis Cells (SElySOs)
* Defektspinelle als Hochenergie- und Hochleistungsmaterialien zur elektrochemischen Energiespeicherung (DESIREE)
* Metall/Luft Systeme, insbesondere Al/- Si/Luft Batterien (AlSiBat)
* Zink/Luft-Batterien mit neuartigen Materialien für die Speicherung regenerativer Energien und die Netzstabilisierung (LuZi)

### Key infrastructure

* Test stations for high temperature electrochemistry
* Electrochemical Impedance Spectroscopy (EIS)
* Test facilities for low temperature battery testing and electrochemistry
* *in-operando* and *in-situ* magnetic resonance spectroscopy
* *in-operando* and *in-situ* electron microscopy

<figure> <img src="{{ site.baseurl }}/img/iek9.png" width="100%"> </figure>

### Contributions to VILLAS project
* Phase 1
	* Investigation of the high temperature steam electrolysis process (SOEC)
	* Performance data generation for use in simulations tools by partners
