---
layout: page
title: "ISEA"
description: "Institute for Power Electronics and Electrical Drives, RWTH Aachen University"
header-img: "img/home-bg.jpg"
order: 12
permalink: /members/isea/
member: isea
---

### Key areas of expertise

* Energy Storage Technologies
* Power Electronics

### Related Projects

* Modular Multimegawatt Multitechnology Medium Voltage Battery (M5Bat)
* Econnect Germany (PV home storage with variable operating strategies)
* Monitoring of PV home storage systems in field operation (WMEP)
* Gewerblich operierende Elektrokleinflotten (GoELK)
* Flexible Electric Networks of the Future (FEN)
* Micro grid with high share of renewable generation + storage (TILOS)
* Nutzen von PV-Heimspeichern für Betreiber und Netz (PV-Nutzen)

### Key infrastructure/tools

* 5 MW / 5 MWh Grid Connected Energy Storage System
* PV Home Storage Systems from various manufacturers
* Test bench for PV Home Storage Systems including PV and load simulation
* Containerized Test-Center equipped with multiple Test-Benches for Battery-Cells and Battery-Packs
* Post mortem laboratory for characterization of battery degradation
* Modular simulation toolbox for power flow simulation of different power supply systems including generation and storage
* Electric vehicles of various manufacturers
* Wall-box
* Bi-directional charger for electric vehicles (under development)

### Contributions to VILLAS project

* Provide experience regarding grid-connected storage systems
* Provide infrastructure to be integrated into VILLAS prototype
	* PV Home Storage System as one Demonstrator of the Villas Project
	* Test bench for PV home storage systems
	* Wall-box
	* Bi-directional charger for electric vehicles
	* Electric vehicles
* Use existing model toolbox as a basis for power flow simulations of various energy systems including generation and storage systems
* Define test cases and perform tests at the villas prototype
* Acquire partners to attach their infrastructure to the VILLAS environment
