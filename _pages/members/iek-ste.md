---
layout: page
title: "IEK-STE"
description: "Institute of Energy and Climate Research – Systems Analysis and Technology Evaluation (IEK-STE)"
header-img: "img/home-bg.jpg"
order: 5
permalink: /members/iek-ste/
member: iek-ste
---

### Contributions to VILLAS project

* Phase I
	* Offer scenarios for long-term applications within energy systems
	* Model application to generate a framework for partners simulation
	* Provide information about public perception
* Phase II
	* Support project acquisition allowing for a large-scale implementation of the VILLAS concept
