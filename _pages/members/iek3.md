---
layout: page
title: "IEK-3"
description: "Institute of Energy and Climate Research Electrochemical Process Engineering, Forschungszentrum Jülich"
header-img: "img/home-bg.jpg"
order: 5
permalink: /members/iek3/
member: iek3
---

### Key areas of expertise

* Water Electrolysis
* Fuel Cells
* Fuel Processing
* System Analysis

### Related Projects

* EKOLYSER
* NestPEL
* Mapel

### Key infrastructure

* Test benches for low temperature water electrolysis
* Test benches for components for water electrolysis
* Test benches for fuel cells
* Pilot plant for manufacturing techniques
* Models for system simulation (Matlab)

<figure> <img src="{{ site.baseurl }}/img/iek3.png" width="100%"> </figure>

### Contributions to VILLAS project
* Phase 1
	* Reinforce the cooperation and the data exchange with RWTH Aachen
	* Establishing of a server connected to VILLAS infrastructure
	* Discussion of safety issues to ensure secure experiments
* Phase 2
	* Integration of hardware into the loop
