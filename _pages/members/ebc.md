---
layout: page
title: "EBC"
description: "Institute for Energy Efficient Buildings and Indoor Climate, RWTH Aachen University"
header-img: "img/home-bg.jpg"
order: 8
permalink: /members/ebc/
member: ebc
---

### Key areas of expertise

* Hardware in the Loop for building energy systems
* Modeling and Simulation of building energy systems and city quarters

### Related Projects

* Dynamic Testing methods for heat pump systems
* Development of Energy Management using HiL (PV-KWK)
* Development of advanced heat pump control using HiL (MOSKWA)
* Test bed for cloud based Smart Energy Services (SCoOP)
* Future Internet Technology for Smart Energy (FINESCE)

### Key infrastructure/tools

* Simulation of building energy systems, building physics and thermal grids in city quarters (Dymola/Modelica)
* 2 Hardware in the Loop test benches for building energy systems (including hydraulic test bench and climate chamber)
* Hardware in the Loop test bench for heat distribution systems and their control on room level

<figure> <img src="{{ site.baseurl }}/img/ebc.png" width="100%"> </figure>

### Contributions to VILLAS project
* Phase I
	* Requirements for Hardware in the Loop of thermal/hydraulic systems
	* Coupling of real-time simulations and test benches via SQL data base
* Phase II
	* Real-time simulation of buildings and/or city quarters
	* Coupling with real building infrastructure (Jülich SDE House, E.ON ERC main building)
