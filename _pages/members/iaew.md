---
layout: page
title: "IAEW"
description: "Institute of Power Systems and Power Economics, RWTH Aachen University"
header-img: "img/home-bg.jpg"
order: 10
permalink: /members/iaew/
member: iaew
---

### Key areas of expertise

* simulation of grid operation and congestion management of the European transmission grid
* unit commitment optimization for Europe
* integrated simulation of electricity, gas and heat markets
* system integration of renewable energy sources and energy storages
* market analysis and price forecasts
* system stability of transmission and distribution grids
* grid expansion planning
* supply quality and security of supply for electrical grids

### Related Projects

* Kopernikus für die Energiewende
* Designetz: Baukasten Energiewende
* Der nächste groﬂe Schritt der Energiewende (enera)
* Flexible Electrical Networks of the Future (FEN)
* Reservemärkte im Wandel - Neue Konzepte für mehr Versorgungssicherheit (ReWal)
* Innovative Netzsicherheitsrechnung (UMBRELL)
* Grid Integrated Multi Megawatt High Pressure Alkaline Electrolysers for Energy Applications (ELYntegration)

### Key infrastructure/tools

* market simulation
* grid models of the European transmission grid and German 110 kV grid
* optimization framework for transmission grid operation simulation (ZKNOT)
* tools for probabilistic generation of German medium and low voltage grids
* target grid planning tool for distribution grids
* optimization of required distribution grid expansion needs in Germany

<figure> <img src="{{ site.baseurl }}/img/iaew.png" width="100%"> </figure>

### Contributions to VILLAS project
* Phase I
	* Support other institutes with experience on grid models for transmission and distribution grids
	* Support other institutes with expert knowledge on optimization frameworks combining market and grid simulations for integration into the overall VILLAS interconnected facilities
* Phase II
	* Contribute to project acquisition allowing further development of the VILLAS concept
