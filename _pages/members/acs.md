---
layout: page
title: "ACS"
description: "Institute for Automation of Complex Power Systems, RWTH Aachen University"
header-img: "img/home-bg.jpg"
order: 4
permalink: /members/acs/
member: acs
---

### Research areas

* Electrical grid dynamics
* ICT for energy

### Related projects

* FINESCE (Future Internet Technology for Smart Energy)
* SCoOP (Test bed for cloud based Smart Energy Services)
* FLEXMETER, ELSA (Energy Local Storage Advanced system)
* COOPERaTE (Control and Optimization for Energy Positive Neighbourhoods)
* IDE4L (Ideal Grid for All)
* FEN (Flexible Electric Networks of the Future)
* SUCCESS (Securing Critical Energy Infrastructures)

### Key infrastructure

* Real-time simulators for power systems (RTDS and OPAL-RT)
* Real-time simulation of distributed system, used e.g. for simulation of wind farms (DSP Cluster)
* Real-time simulation of multi-physic systems, use e.g. for integrated simulation of electrical and thermal system (PC cluster)
* Power Hardware In the Loop interface, used for connection of real hardware (up to 25 kVA) in the control loop (Flexible Power Simulator FlePS)
* Open Stack installation for Cloud Applications

<figure> <img src="{{ site.baseurl }}/img/acs.png" width="100%"> </figure>

### Contributions to the project

* Develop VILLAS prototype architecture linking three laboratories
* Develop web-based interface for visualization and user interaction for VILLAS prototype and coordinate its demonstration
* Offer infrastructure capable of real-time simulation of power systems
* Support other institutes with experience on cloud-based applications and  development of the required VILLAS interface for their laboratories or databases for integration into the overall VILLAS interconnected facilities
* Drive project acquisition for VILLAS phase 2 allowing for a large-scale implementation of the VILLAS concept
* Coordinate VILLAS infrastructure specification (including specification of the use-cases)
* Develop information material and website
* Coordinate preparation of the project report
