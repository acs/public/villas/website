---
layout: page
title: "CWD"
description: "Center for Wind Power Drives, RWTH Aachen University"
header-img: "img/home-bg.jpg"
order: 7
permalink: /members/cwd/
member: cwd
---

### Key areas of expertise/research

* Wind Turbine Generator (WTG) system analysis and design within  simulation and testing

### Sample Projects

* Gondel (design & operating a research WTG on 4 MW system test bench, measurements and tool validation)
* Rapid Wind (6 MW WTG with multiple high speed generators)
* Hydrola

### Key infrastructure/tools

* 4 MW system test bench with
	* 4 MW PM DD prime mover
	* Wind load application system
	* 22 MVA grid emulation system
	* Hardware in the Loop  control system for rotor system emulation

<figure> <img src="{{ site.baseurl }}/img/cwd.png" width="100%"> </figure>

### Contributions to VILLAS project

* Develop the software link between E.On RTDS grid emulation system on software level & 22 MVW grid emulation on hardware level at CWD
