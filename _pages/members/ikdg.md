---
layout: page
title: "IKDG"
description: "Institute of Power Plant Technology, Steam and Gas Turbines, RWTH Aachen University"
header-img: "img/home-bg.jpg"
order: 11
permalink: /members/ikdg/
member: ikdg
---

### Key areas of expertise

* Gas turbine combustion technology
* Stream turbine technology
* Power plant process modeling, analysis and optimization

### Related Projects

* Profiles with advanced tangential end wall contouring
* Pre-warming of steam turbine components
* Long-term analysis of emissions and combustion chamber pulsation for operational monitoring and optimization
* Future municipal energy supply systems

### Key infrastructure/tools

* High pressure stream turbine test rigs
* Gas turbine combustor test rig
* Exhaust gas turbo charger test rigs
* Real-time power plant simulator

<figure> <img src="{{ site.baseurl }}/img/ikdg.png" width="100%"> </figure>

### Contributions to VILLAS project
* Provision of extensive large-scale turbomachinery and combustion test rig infrastructure for potential integration into the VILLAS network
