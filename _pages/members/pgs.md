---
layout: page
title: "PGS"
description: "Institute for Power Generation and Storage Systems, RWTH Aachen University"
header-img: "img/home-bg.jpg"
order: 13
permalink: /members/pgs/
member: pgs
---

### Key areas of expertise

* Power-electronics devices
* Power-electronic converters and electrical drives for high-power applications

### Related Projects

* MVDC Collector Grid for Offshore Wind Farms
* Medium-Frequency High-Power DC-DC Converter
* Flexible Electrical Network (FEN)
* FVA-Nacelle

### Key infrastructure/tools

* Facilites for power-electronic devices:
	* Clean room
	* Test-bench for characterization of power-electronics devices under normal and severe stress conditions
* 5 MW medium-voltage high-speed laboratory:
	* Testing power-electronic converters and electrical drives up to 3.3 kV (ac), 5 kV (dc) and 15.000 rpm
* 5 MW medium-voltage dc-dc converter
* System-level wind-turbine test bench at the Center for Wind Power Drives

<figure> <img src="{{ site.baseurl }}/img/pgs.png" width="100%"> </figure>

### Contributions to VILLAS project

* Modelling of power-electronic converters for off-line and real-time simulation
* Developing and implementation of the control algorithm for the power-electronic converters considered in the VILLAS structure
* Development of the interface between the converter controller and the real-time simulation platform
* Analysis of the power-electronic converters behavior in the system
